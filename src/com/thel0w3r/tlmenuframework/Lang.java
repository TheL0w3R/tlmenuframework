package com.thel0w3r.tlmenuframework;

import java.util.HashMap;
import java.util.Map;

/**
 * Class that manages languages.
 * @author TheL0w3R
 */
public class Lang {

    private Locale locale;
    private boolean usingCustom;
    private HashMap<Locale, HashMap<String, String>> dic;

    /**
     * The constructor, sets up the language translations and custom ones.
     * @param locale The chosen locale.
     * @param usingCustom A variable to check if the user specified custom translations.
     * @param customDic The custom dictionary built by the user (empty by default).
     */
    public Lang(Locale locale, boolean usingCustom, HashMap<String, String> customDic) {
        this.locale = locale;
        this.usingCustom = usingCustom;
        this.dic = new HashMap<>();

        dic.put(Locale.US, new HashMap<>());
        dic.put(Locale.ES, new HashMap<>());
        dic.put(Locale.IT, new HashMap<>());

        if(usingCustom)
            dic.put(Locale.CUSTOM, new HashMap<>());

        dic.get(Locale.US).put("invalidMsg", "That's not a valid option.");
        dic.get(Locale.US).put("exitOption", "Exit.");
        dic.get(Locale.US).put("selectMsg", "Select an option");

        dic.get(Locale.ES).put("invalidMsg", "No es una opción válida.");
        dic.get(Locale.ES).put("exitOption", "Salir.");
        dic.get(Locale.ES).put("selectMsg", "Elige una opción");

        dic.get(Locale.IT).put("invalidMsg", "Non è un'opzione valida.");
        dic.get(Locale.IT).put("exitOption", "Chiudere.");
        dic.get(Locale.IT).put("selectMsg", "Seleziona un'opzione");

        if(usingCustom) {
            dic.get(Locale.CUSTOM).put("invalidMsg", dic.get(this.locale).get("invalidMsg"));
            dic.get(Locale.CUSTOM).put("exitOption", dic.get(this.locale).get("exitOption"));
            dic.get(Locale.CUSTOM).put("selectMsg", dic.get(this.locale).get("selectMsg"));

            for(Map.Entry<String, String> e : customDic.entrySet()) {
                dic.get(Locale.CUSTOM).put(e.getKey(), e.getValue());
            }
        }
    }

    /**
     * Method to get by key the wanted text in the selected language.
     * @param key The key that specifies what text you want.
     * @return The wanted text translated in the wanted language.
     */
    public String getText(String key) {
        return (usingCustom) ? dic.get(Locale.CUSTOM).get(key) : dic.get(this.locale).get(key);
    }

}
