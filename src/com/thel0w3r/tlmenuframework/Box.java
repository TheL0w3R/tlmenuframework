package com.thel0w3r.tlmenuframework;

/**
 * Enum for existing box segments.
 * @author TheL0w3R
 */
public enum Box {
    UPLEFT, HORIZONTAL, UPRIGHT, VERTICAL, DOWNLEFT, DOWNRIGHT, ENDRIGHT, ENDLEFT
}
