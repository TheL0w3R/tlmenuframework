package com.thel0w3r.tlmenuframework;

import java.util.Scanner;

/**
 * Abstract class that represents a Menu entry.
 * @author TheL0w3R
 */
public abstract class MenuOption {

    private String optionName;
    public Scanner sc;

    /**
     * The constructor, used to set the entry text for the menu and initializing the Scanner.
     * @param optionName The text displayed on the menu.
     */
    public MenuOption(String optionName) {
        sc = new Scanner(System.in);
        this.optionName = optionName;
    }

    /**
     * Method to get the entry text of the current MenuOption instance.
     * @return The entry text.
     */
    public String getOptionName() {
        return optionName;
    }

    /**
     * Abstract method that needs to be overriden by it's child, it's called when the current option is selected.
     */
    public abstract void run();

}
