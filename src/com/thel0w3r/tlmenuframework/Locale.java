package com.thel0w3r.tlmenuframework;

/**
 * Enum for existing locales.
 * @author TheL0w3R
 */
public enum Locale {
    ES, US, IT, CUSTOM
}
