package com.thel0w3r.tlmenuframework;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Class to easily construct the menu.
 * @author TheL0w3R
 */
public class MenuBuilder {

    private String menuTitle;
    private HashMap<String, String> customDic;
    private int exitNum;
    private Locale locale;
    private BoxChars boxChars;
    private ArrayList<MenuOption> options;

    /**
     * The constructor, used to initialize all the default Menu properties.
     */
    public MenuBuilder() {
        menuTitle = "";
        customDic = new HashMap<>();
        exitNum = 99;
        locale = Locale.US;
        boxChars = new BoxChars(false, false, '*');
        options = new ArrayList<>();
    }

    /**
     * Method to build the Menu object.
     * @return The instance of Menu with all the given properties.
     */
    public Menu build() {
        return new Menu(boxChars, menuTitle, exitNum, locale, customDic, options);
    }

    /**
     * Method to set a custom character as border (IF BOX-DRAWING IS DISABLED).
     * @param c The custom char.
     * @return Itself (part of the build pattern).
     */
    public MenuBuilder borderChar(char c) {
        boxChars.setSurrChar(c);
        return this;
    }

    /**
     * Method to set a custom Menu title.
     * @param s The custom title.
     * @return Itself (part of the build pattern).
     */
    public MenuBuilder title(String s) {
        this.menuTitle = s;
        return this;
    }

    /**
     * Method to add an option.
     * @param o A MenuOption instance.
     * @return Itself (part of the build pattern).
     */
    public MenuBuilder addOption(MenuOption o) {
        this.options.add(o);
        return this;
    }

    /**
     * Method to set a custom error message when the selected option isn't valid.
     * @param msg The custom message.
     * @return Itself (part of the build pattern).
     */
    public MenuBuilder invalidMessage(String msg) {
        this.customDic.put("invalidMsg", msg);
        return this;
    }

    /**
     * Method to set a custom 'exit' option displayed in the Menu.
     * @param msg The custom text.
     * @return Itself (part of the build pattern).
     */
    public MenuBuilder exitOption(String msg) {
        this.customDic.put("exitOption", msg);
        return this;
    }

    /**
     * Method to set a custom number for the 'exit' option.
     * @param num The custom number.
     * @return Itself (part of the build pattern).
     */
    public MenuBuilder exitNum(int num) {
        this.exitNum = num;
        return this;
    }

    /**
     * Method to set a custom message for the 'select an option' prompt.
     * @param msg The custom message.
     * @return Itself (part of the build pattern).
     */
    public MenuBuilder selectMsg(String msg) {
        this.customDic.put("selectMsg", msg);
        return this;
    }

    /**
     * Method to set the language (US by default).
     * @param l The custom language.
     * @return Itself (part of the build pattern).
     */
    public MenuBuilder setLocale(Locale l) {
        this.locale = l;
        return this;
    }

    /**
     * Method to enable or disable the Box-Drawing characters (SUPPORTED ONLY ON MONOSPACED FONTS).
     * @param b The state of the Box-Drawing characters.
     * @return Itself (part of the build pattern).
     */
    public MenuBuilder useBoxChars(boolean b) {
        this.boxChars.setUseBoxChars(b);
        return this;
    }

    /**
     * Method to enable or disable the double border box.
     * @param b The state of the double border box.
     * @return Itself (part of the build pattern).
     */
    public MenuBuilder doubleBox(boolean b) {
        this.boxChars.setDoublebox(b);
        return this;
    }
}
