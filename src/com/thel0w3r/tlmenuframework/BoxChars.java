package com.thel0w3r.tlmenuframework;

/**
 * Class to get the right character based on user preferences.
 * @author TheL0w3R
 */
public class BoxChars {

    private boolean useBoxChars;
    private boolean doublebox;
    private char surrChar;

    /**
     * The constructor, used to initialize all the custom props.
     */
    public BoxChars(boolean useBoxChars, boolean doublebox, char surrChar) {
        this.useBoxChars = useBoxChars;
        this.surrChar = surrChar;
        this.doublebox = doublebox;
    }

    /**
     * Method to get the right character based on user preferences.
     * @param seg The segment we want our character to fill, useful to set up a box.
     * @return The correct character based on user preferences and the wanted segment.
     */
    public char gc(Box seg) {
        if(!useBoxChars)
            return surrChar;

        char tmp = surrChar;

        switch(seg) {
            case UPLEFT:
                tmp = (char) ((doublebox) ? 0x2554 : 0x250C);
                break;
            case HORIZONTAL:
                tmp = (char) ((doublebox) ? 0x2550 : 0x2500);
                break;
            case UPRIGHT:
                tmp = (char) ((doublebox) ? 0x2557 : 0x2510);
                break;
            case VERTICAL:
                tmp = (char) ((doublebox) ? 0x2551 : 0x2502);
                break;
            case DOWNLEFT:
                tmp = (char) ((doublebox) ? 0x255A : 0x2514);
                break;
            case DOWNRIGHT:
                tmp = (char) ((doublebox) ? 0x255D : 0x2518);
                break;
            case ENDLEFT:
                tmp = (char) ((doublebox) ? 0x2563 : 0x2524);
                break;
            case ENDRIGHT:
                tmp = (char) ((doublebox) ? 0x2560 : 0x251C);
                break;
            default:
                tmp = surrChar;
                break;
        }

        return tmp;
    }

    /**
     * Wrapper method to parse the character as a String before returning it, to make the Menu code more readable.
     * @param seg The segment we want our character to fill, useful to set up a box.
     * @return The correct character based on user preferences and the wanted segment.
     */
    public String gcs(Box seg) {
        return String.valueOf(gc(seg));
    }

    /**
     * Method to set the surrounding char to be used as a fallback or as border character if Box-Drawing is disabled.
     * @param c The wanted character.
     */
    public void setSurrChar(char c) {
        this.surrChar = c;
    }

    /**
     * Method to enable or disable the usage of Box-Drawing characters.
     * @param b The state of the Box-Drawing characters option.
     */
    public void setUseBoxChars(boolean b) {
        this.useBoxChars = b;
    }

    /**
     * Method to enable or disable the double-lined characters.
     * @param b The state of the double-lined characters option.
     */
    public void setDoublebox(boolean b) {
        this.doublebox = b;
    }
}
