package com.thel0w3r.tlmenuframework;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * Class that represents the Menu.
 * @author TheL0w3R
 */
public class Menu {

    //TODO: Add utility methods like the string menu view (without functionality, only layout).
    private HashMap<Integer, MenuOption> menu;
    private StringBuilder sb;
    private int exitNum;
    private ArrayList<MenuOption> o;

    private Scanner sc;
    private Lang lang;

    /**
     * The Menu object constructor, it creates the Menu prompt string.
     * @param boxChars The preconfigured box character manager instance.
     * @param mTitle The title of the Menu.
     * @param eNum The number to trigger the 'exit' option.
     * @param loc The selected language.
     * @param customDic The custom dictionary for translations (CAN BE EMPTY).
     * @param o An array of MenuOption instances (the menu entries).
     */
    public Menu(BoxChars boxChars, String mTitle, int eNum, Locale loc, HashMap<String, String> customDic, ArrayList<MenuOption> o) {
        menu = new HashMap<>();
        sb = new StringBuilder();
        this.exitNum = eNum;
        this.o = o;

        lang = new Lang(loc, !(customDic.isEmpty()), customDic);
        sc = new Scanner(System.in);

        int maxChars = 20;

        if(mTitle.length() > maxChars)
            maxChars = mTitle.length() + 6;
        else if(lang.getText("exitOption").length() + 7 + Integer.toString(eNum).length() > maxChars)
            maxChars = lang.getText("exitOption").length() + 7 + Integer.toString(eNum).length() + 6;

        for(MenuOption op : o) {
            int tmp = op.getOptionName().length();
            if(tmp > maxChars)
                maxChars = tmp;
        }

        int menuLength = maxChars + (maxChars/3);

        int titleLength = (mTitle.length() >= 1) ? (menuLength - 2 - mTitle.length()) : menuLength - 2;

        sb.append(boxChars.gc(Box.UPLEFT));
        sb.append(getStr((titleLength / 2) - 2, boxChars.gcs(Box.HORIZONTAL))).append((mTitle.length() >= 1) ? boxChars.gc(Box.ENDLEFT) : boxChars.gc(Box.HORIZONTAL)).append((mTitle.length() >= 1) ? " " : "").append(mTitle)
                .append((mTitle.length() >= 1) ? " " : "").append((mTitle.length() >= 1) ? boxChars.gc(Box.ENDRIGHT) : boxChars.gc(Box.HORIZONTAL));

        if(titleLength % 2 == 0)
            sb.append(getStr((titleLength / 2) - 2, boxChars.gcs(Box.HORIZONTAL)));
        else
            sb.append(getStr((titleLength / 2) - 1, boxChars.gcs(Box.HORIZONTAL)));

        sb.append((mTitle.length() <= 1) ? getStr(2, boxChars.gcs(Box.HORIZONTAL)) : "").append(boxChars.gc(Box.UPRIGHT)).append("\n");
        sb.append(boxChars.gc(Box.VERTICAL)).append(getStr(menuLength - 2, " ")).append(boxChars.gc(Box.VERTICAL)).append("\n");

        for(int i = 1; i <= o.size(); i++) {
            menu.put(i, o.get(i-1));
            int l = (menuLength - o.get(i-1).getOptionName().length() - 1) - (6 + String.valueOf(i).length());
            sb.append(boxChars.gc(Box.VERTICAL)).append("  ").append(i).append(" - ").append(o.get(i - 1).getOptionName())
                    .append(getStr((l > 0) ? l : 0, " ")).append(boxChars.gc(Box.VERTICAL)).append("\n");
        }

        sb.append(boxChars.gc(Box.VERTICAL)).append("  ").append(eNum).append(" - ").append(lang.getText("exitOption"))
                .append(getStr(menuLength - (7 + Integer.toString(eNum).length() + lang.getText("exitOption").length()), " "))
                .append(boxChars.gc(Box.VERTICAL)).append("\n");
        sb.append(boxChars.gc(Box.VERTICAL)).append(getStr(menuLength - 2, " ")).append(boxChars.gc(Box.VERTICAL)).append("\n");
        sb.append(boxChars.gc(Box.DOWNLEFT)).append(getStr(menuLength - 2, boxChars.gcs(Box.HORIZONTAL))).append(boxChars.gc(Box.DOWNRIGHT)).append("\n");
        sb.append("\n").append(lang.getText("selectMsg")).append(": ");
    }

    /**
     * Method to run the Menu (on the main thread).
     */
    public void start() {
        while(true) {
            System.out.print(sb.toString());
            try {
                int option = sc.nextInt();
                System.out.println("");
                if(option > 0 && option <= o.size()) {
                    Thread t = new Thread(() -> {
                        o.get(option - 1).run();
                    });
                    t.start();
                    t.join();
                } else if(option == exitNum) {
                    return;
                } else {
                    System.out.println(lang.getText("invalidMsg"));
                }
                System.out.println("");
            } catch (InputMismatchException e) {
                sc.nextLine();
                System.out.println("\n" + lang.getText("invalidMsg") + "\n");
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    private String getStr(int l, String c) {
        return new String(new char[l]).replace("\0", c);
    }

}
